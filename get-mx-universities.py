#
# Copyright (c) 2019 Filipe Saraiva <saraiva@ufpa.br>
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT)
#

import sys
import time
import subprocess
import json
import ipinfo
import socket
import shutil
import operator

'''
The `providers` variable stores the information used by the script. The model is
like:

'PROVIDER' : [0, 'sub1', 'sub2', sub3', ...],

Where:

    * 'PROVIDER' is the name of the provider you wish to search. It is used just
                 to present information by the script;
    
    * 0 is a mandatory value you must to put as the first element of the list;
    
    * 'sub1', 'sub2', 'sub3', ... are the substring you need to provide in order
                 to script look for these in the output lines. You must to inform
                 some common parts of the server utilized by the Provider. For
                 example, it is common for Google to use 'googlemail', 'gmail',
                 'google' or others to its email server. So, we put 'google' and
                 'gmail' to the script. All the substrings must be in lower case
                 letters.
    
It is easy to extend the `providers` variable -- just add a new line like these
present in the variable. Copy-past some of them, inform a new 'PROVIDER', keep
the 0 at the first space, and change the substrings to those you wish to use in
the script.
'''
providers = {
    'Google' : [0, 'google', 'gmail'],
    'Microsoft' : [0, 'microsoft', 'outlook'],
    'Error' : [0, 'error', 'error']
    }

'''
From this point is the part not configurable of the script. Unless you know
what you are doing, we recommend do not touch it.
'''

try:
    hosts = open(sys.argv[1]).read().splitlines()
except:
    print('File', sys.argv[1], 'not found')
    exit()

outputFileName = 'output-' + time.strftime('%Y%m%d') + '-' + sys.argv[1]
outputFile = open(outputFileName, 'w')

'''
JSON Database elements
{
    "institution.example":{
        "initial": "E" | "F" | "I" | "S",
        "latitude": "",
        "longitude":"",
        "state": "",
        "mx":"",
        "gafam": True | False,
        "provider": "Google" | "Microsoft" | "Outro"
    }
}
'''

db = None
name_db = None
db_true = '--db' in sys.argv
handler = None
db_changes = []

if db_true:
    try:
        name_db = sys.argv[sys.argv.index('--db')+1]
        db = json.load(open(name_db))
        handler = ipinfo.getHandler()
    except:
        print('Error load json database')
        exit()

initial = ''
if '--initial' in sys.argv:
    try:
        initial = sys.argv[sys.argv.index('--initial')+1]
    except:
        print('Error read initial')
        exit()

title = ''
if '--title' in sys.argv:
    try:
        title = sys.argv[sys.argv.index('--title')+1]
    except:
        print('Error read title')
        exit()

logProvider = '\n\n##### Provider Log #####\n\n'
log = '\n##### Output Log #####\n\n'
logError = '\n##### Error Log #####\n\n'

for host in hosts:
    outputCommand = subprocess.run(['host', '-t', 'MX', host], encoding='utf-8', stdout=subprocess.PIPE)
    
    if not outputCommand.stdout.lower().__contains__(' mail '):
        print('Error in host ' + host)
        providers['Error'][0] += 1
        logError += 'Error in host ' + host + '\n'
        logProvider += host + ' ERROR\n'
        continue
    
    listMXOutput = outputCommand.stdout.lower().splitlines()
    listMXOutput.sort(key=lambda x : (int(x.split()[5]), x.split()[6]))
    
    for line in listMXOutput:
        if ' mail ' in line:
            log += line + '\n'
            print(line)
            
            if db_true:
                if host not in db:
                    db[host] = { 'initial':initial }

                element = db.get(host)
                element['mx'] = line.split()[-1] if not element.get('mx') else element['mx']
                element['initial'] = initial if initial else element['initial']
                
                current_gafam = False if not element.get('gafam') else element['gafam']
                current_provider = 'Outro' if not element.get('provider') else element['provider']
                
                element['gafam'] = False
                element['provider'] = 'Outro'
                
                if any( not element.get(key) for key in ['latitude','longitude','state'] ):
                    try:
                        ip = socket.gethostbyname(host)
                        details = handler.getDetails(ip).all
                        state,latitude,longitude = map(details.get,['region','latitude','longitude'])
                        
                        element['state'] = state if not element.get('state') else element['state']
                        element['latitude'] = latitude if not element.get('latitude') else element['latitude']
                        element['longitude'] = longitude if not element.get('longitude') else element['longitude']
                    except:
                        element['state'] = '' if not element.get('state') else element['state']
                        element['latitude'] = '' if not element.get('latitude') else element['latitude']
                        element['longitude'] = '' if not element.get('longitude') else element['longitude']
            
            isOthers = True
            for provider in providers:
                if any(sub in line for sub in providers[provider][1:]):
                    isOthers = False
                    providers[provider][0] += 1
                    logProvider += host + ' ' + provider + '\n'
                    if db_true:
                        element = db.get(host)
                        element['gafam'] = True
                        element['provider'] = provider
                    break
            
            if isOthers:
                logProvider += host + ' Others\n'
            
            if db_true:
                element = db.get(host)
                if element.get('gafam') != current_gafam or element.get('provider') != current_provider:
                    element['mx'] = line.split()[-1]
                    db_changes.append('<p>* '+host+' mudou de '+current_provider+' ('+str(current_gafam)+') para '+element.get('provider')+' ('+str(element.get('gafam'))+')</p>')
            break

if db_true:
    if db_changes:
        name_db_dst = ''.join(name_db.split('.')[:-1])+'-'+time.strftime('%Y%m%d')+'.json'
        
        shutil.copyfile( name_db, name_db_dst) # bkp
        
        with open(name_db, 'w') as outfile:
            json.dump(db, outfile, indent=4)
        
        with open('general-report.html','a+') as outfile:
            outfile.write('\n')
            if title:
                outfile.write('\n<h3>'+title+'</h3>\n\n')
            outfile.write( '\n'.join(db_changes) )

    elements_gafam = list()
    elements_n_gafam = list()
    
    for host in db:
        element = db.get(host)
        obj_tmp = {
            'type': 'Feature',
            'properties': {
                'name': host,
                'initial': element['initial'],
                'state': element['state'],
                'gafam': element['gafam'],
                'mx': element['mx'],
                'provider': element['provider']
            },
            'geometry': {
                'type': 'Point',
                'coordinates': [
                    element['longitude'],
                    element['latitude']
                ]
            }
        }
        if element.get('gafam'):
            elements_gafam.append(obj_tmp)
        else:
            elements_n_gafam.append(obj_tmp)
    
    maps = {
        'gafam':{'type': 'FeatureCollection','features': elements_gafam},
        'n-gafam':{'type': 'FeatureCollection','features': elements_n_gafam}
    }
    
    name_base = name_db.split('/')[-1].split('.')[0]
    
    for name in maps:
        with open('map/'+name_base+'-'+name+'.geojson', 'w+') as outfile:
            json.dump(maps[name], outfile, indent=4)

numProvided = 0
for i in providers:
    numProvided += providers[i][0]

numHosts = len(hosts)
numOthers = numHosts - numProvided

resume = '##### Resume #####\n'
for i in providers:
    resume += '\n%s: %d (%0.2f%%)' % (i, providers[i][0], providers[i][0] * 100 / numHosts)

resume += '\nOthers: %d (%0.2f%%)' % (numOthers, numOthers * 100 / numHosts)
resume += '\nTotal: %d' % numHosts

print('\n' + resume)
print('\nLog saved to %s file' % (outputFileName))

outputFile.write(resume)
outputFile.write(logProvider)
outputFile.write(log)

if (providers['Error'][0] > 0):
    outputFile.write(logError)
    
outputFile.close()

'''
Code block to write JSON output
'''

outputJsonName = sys.argv[1].replace('.txt', '.json')
outputJson = open(outputJsonName, 'w')

providers['Google'] = providers['Google'][0]
providers['Microsoft'] = providers['Microsoft'][0]
providers['Others'] = numOthers
providers['Total'] = numHosts

outputJson.write(json.dumps(providers))
outputJson.close()

print('JSON output saved to %s file\n' % (outputJsonName))
